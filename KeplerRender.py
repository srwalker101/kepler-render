#!/usr/bin/env python

import sys
import pyfits
import argparse
import os
import numpy as np
import re
import requests
from urllib import urlretrieve
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from functools import partial
import tempfile
import json
import scipy.signal as signal

def bandpass_filter(xdata, ydata, period):
    nyquist = np.average(np.diff(xdata)) / 2.

    pass_range = 0.5

    a = signal.firwin(61, pass_range, window='hamming')
    newydata = signal.lfilter(a, 1, ydata)
    return newydata





def get_info_from_mapping(koi):
    '''
    Use the mapping file to retrieve the kepler information
    '''
    with open(os.path.join(os.path.dirname(__file__),
        'mapping.json'), 'rb') as mappingfile:

        try:
            object_info = json.load(mappingfile)[koi]
        except KeyError:
            raise RuntimeError('cannot find koi \'{0}\''.format(koi))

    return object_info

def verify_koi(koi):
    '''
    Makes sure the koi is sane
    '''
    return bool(re.search(r'^\d{1,6}\.\d{2}', koi))

def fit_fn(x, a, b, c):
    '''
    second order polynomial
    '''
    return a * x ** 2 + b * x + c

def to_phase(mjd, period, epoch):
    phase = (np.abs(mjd - epoch) / period) % 1
    phase[phase > 0.8] -= 1.0
    return phase

def oot_data(phase, duration, period):
    duration = duration / period
    ind = (phase < -duration / 2.) | (phase > duration / 2.)
    return ind

def correct(phase, flux, fluxerr, ootind):
    '''
    Fits a second order polynomial to the out of transit data and
    correct the lightcurve
    '''
    ootphase, ootflux, ootfluxerr = [d[ootind] for d in [phase, flux, fluxerr]]
    # Fit the polynomial

    popt, pcov = curve_fit(fit_fn, ootphase, ootflux, sigma=ootfluxerr)
    model_data = fit_fn(phase, *popt)


    return model_data

def jd2mjd(jd):
    return jd - 2400000.5

def bin(xdata, ydata, errdata, nbins, x_range=(-0.2, 0.8)):
    '''
    Bin the data into an integer number of bins
    '''
    xdata, ydata, errdata = [np.array(d) for d in (xdata, ydata, errdata)]
    inc = (x_range[1] - x_range[0]) / float(nbins)

    bx, by, be = [], [], []
    for i in xrange(nbins - 1):
        l = x_range[0] + i * inc
        h = l + inc

        ind = (xdata >= l) & (xdata < h)
        bx.append(l)

        inbin_y = ydata[ind]
        inbin_err = errdata[ind]

        try:
            bin_av = np.average(inbin_y, weights=1. / inbin_err ** 2)
            by.append(bin_av)
            be.append(1.25 * np.median(np.abs(inbin_y - np.median(inbin_y))) /
                    np.sqrt(inbin_y.size))
        except ZeroDivisionError:
            by.append(0)
            be.append(0)

    return [np.array(d) for d in [bx, by, be]]


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('koi')
    parser.add_argument('-n', '--nbins', required=False, default=200,
            help='Number of phase bins', type=int)
    parser.add_argument('-t', '--transit', required=False, action='store_true',
            help='Only show the transit')
    parser.add_argument('-r', '--raw', action='store_true', default=False,
            help='Plot raw lightcurves')
    parser.add_argument('-s', '--nsigma', required=False, default=3, type=float,
            help='Sigma clip value')
    return parser.parse_args()


def analyse_datafile(filename):
    with pyfits.open(filename) as infile:
        hdu = infile['lightcurve']
        lightcurve_hdu = hdu.data
        mjd_const = int(hdu.header['BJDREFI'])

    mjd = jd2mjd(lightcurve_hdu.field('time') + mjd_const)
    flux = lightcurve_hdu.field('pdcsap_flux')
    fluxerr = lightcurve_hdu.field('pdcsap_flux_err')

    return mjd, flux, fluxerr

def retrieve_datafile(kid):
    base_url = 'http://archive.stsci.edu/pub/kepler/lightcurves'
    KID = '{0:09d}'.format(int(kid))

    base_dir = os.path.join(tempfile.gettempdir(), KID)

    url = os.path.join(base_url, KID[:4], KID)
    r = requests.get(url)

    soup = BeautifulSoup(r.content)
    for link in soup.find_all('a'):
        filename = link.get('href')
        if 'fits' in filename:
            if not os.path.isdir(base_dir):
                os.makedirs(base_dir)

            name = os.path.join(base_dir, filename)

            if os.path.isfile(name):
                print '{0} already exists'.format(filename)
                yield name
            else:
                print 'Retrieving {0}'.format(filename)
                urlretrieve(os.path.join(url, filename), name)
                yield name



def main(args):
    # epoch given is with reference to the date below
    if not verify_koi(args.koi):
        raise RuntimeError('''Invalid KOI value supplied.

KOI is of format dddddd.dd where d is a digit 0-9''')

    object_info = get_info_from_mapping(args.koi)
    kep_jd_ref = 2454900
    epoch = jd2mjd(object_info['epoch'] + kep_jd_ref)
    period = object_info['period']
    duration = object_info['duration'] / 24.
    kid = object_info['kid']

    kic_phase = partial(to_phase, period=period, epoch=epoch)
    kic_oot_data = partial(oot_data, duration=duration, period=period)


    xdata, ydata, errdata = [], [], []
    mdata = []
    for i, filename in enumerate(retrieve_datafile(kid)):
        mjd, flux, fluxerr = analyse_datafile(filename)

        # Remove nans
        goodind = (mjd==mjd) & (flux==flux) & (fluxerr==fluxerr)
        mjd, flux, fluxerr = [d[goodind] for d in [mjd, flux, fluxerr]]


        phase = kic_phase(mjd)

        #flux = bandpass_filter(mjd, flux, period)

        # Fit a second order polynomial to the oot data
        oot_ind = kic_oot_data(phase)

        mflux = correct(mjd, flux, fluxerr, oot_ind)

        if args.raw:
            xdata.extend(mjd)
            mdata.extend(mflux)
            ydata.extend(flux)
            errdata.extend(fluxerr)
        else:
            flux -= mflux


            # Sigma clip the flux based on the residuals
            std_val = np.std(flux)
            sigma_ind = ((flux <= args.nsigma * std_val) &
                    (flux >= -args.nsigma * std_val))

            xdata.extend(phase[sigma_ind])
            ydata.extend(flux[sigma_ind])
            errdata.extend(fluxerr[sigma_ind])



    if args.raw:
        plt.errorbar(xdata, ydata, errdata, ls='None', mec='b')
        plt.plot(xdata, ydata, 'r,', mec='r', ms=5)
        plt.plot(xdata, mdata, 'g,', mec='g', ms=5)
        plt.xlabel(r'MJD')
    else:
        if args.transit:
            max_dur = 3. * duration / period
            max_dur = max_dur if -max_dur > -0.2 else 0.2


            bx, by, be = bin(xdata, ydata, errdata, args.nbins,
                    x_range=(-max_dur, max_dur))
        else:
            bx, by, be = bin(xdata, ydata, errdata, args.nbins)

        plt.errorbar(bx, by, be, ls='None', mec='b', ms=5)
        plt.plot(bx, by, 'r.', mec='r', ms=5)
        plt.xlabel(r'Orbital phase')

    plt.ylabel(r'Normalised flux / $es^{-1}$')
    plt.show()





if __name__ == '__main__':
    try:
        main(create_parser())
    except RuntimeError as err:
        print >> sys.stderr, 'Error: {0:s}'.format(str(err))
